---
title: "This Website and the Web Framework Hugo"
date: 2021-03-19T16:46:04+01:00
# categories: ["Working days"]
# tags: ["work", "day"]
# description: "This is meta description"
# keywords: "meta keywords for content"
draft: false
---

Welcome! I've created this website both as a personal writing space and repository for interesting things I come across during reading, research and work. 

My graduate programme [Responsibility in Science, Engineering and Technology](https://www.elitenetzwerk.bayern.de/en/home/funding-programs/elite-graduate-programs/overview-of-elite-graduate-programs/responsibility-in-science-engineering-and-technology) at the Technical University of Munich critically analyses the central role of science and technology in our modern societies, the changes both bring about for our lives, and asks the questions for power and responsibility in these developments. In this context I've written essays about a venture capitalist's engagement in discourses of biotechnology and CRISPR, on the introduction of Facebook's *Oversight Board*, the popular suggestion to break up *Big Tech* in a similar way to *Big Oil* a century earlier, or the idea to fix post-truth with 'objective' data.

This space is meant to be both practical and experimental, allowing me to pick up on ideas, reflect and develop them further to eventually bring them into an accessible written form.

The design of the website is an hommage to my favourite writing environment and text editor [iA Writer](https://ia.net/writer). Hence, I also used their (freely available) iA Writer Quattro fonts referenced in the footer.

This website is built with the web framework [Hugo](https://gohugo.io/), and the [Console Theme](https://github.com/mrmierzejewski/hugo-theme-console/) created by [Marcin Mierzejewski](https://mrmierzejewski.com/). 

**Hugo** is an open-source project written in the programming language Go and a static site generator. Many websites that for example run on Wordpress (which roughly one third of all websites worldwide do) are dynamic websites. This means these websites are newly rendered again and again when a visitor visits the site. This impacts how fast a site can be loaded on the visitor's device but also has security implications because dynamic websites rely on more systems in the background (e.g., databases, content management systems, …) that better be up to date. Hugo instead builts a website solely from HTML and CSS files that don't need to be recreated every time — hence *static* — and can be hosted on almost any webspace providing server on the web. I, for example, chose to host this website on [GitLab](https://gitlab.com).

In a nutshell, Hugo has saved me from the hassle (and boredom) of setting up a Wordpress-powered site,  giving me a lot of flexibility and an interesting technical challenge learning my way around Bash, Git and the web framework itself.