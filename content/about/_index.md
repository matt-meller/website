+++
date = "2021-03-18"
title = "About"
+++
<style>img{width:12.5em;border-radius:1%;float:right;margin-left:20px;margin-bottom:20px}@media only screen and (max-width:650px){img{width:100%;margin-left:auto;margin-right:auto}}</style>

<img src="/images/01a9ae42a420ac4bad669dc8012c1b51929d1e5c.png">

Hi, my name is **Matthias Meller** and I am Master candidate in the elite graduate programme [Responsibility in Science, Engineering and Technology](https://www.elitenetzwerk.bayern.de/en/home/funding-programs/elite-graduate-programs/overview-of-elite-graduate-programs/responsibility-in-science-engineering-and-technology) at the Technical University of Munich.

My current research interest is centred on the power dimensions, particularly the economic-infrastructural ones, of digital platforms and their implications for society and policy. Furthermore, I'm interested in innovation cultures for example as in (regional) digital transformation strategies, narratives of innovation and (seemingly inevitable) futures as often seen in discussion of "disruptive technologies", and issues of responsibility in computer science (education).

Over the course of the last years I did a number of things:

- attended over 20 national and international startup conferences, speaking to other founders and venture capital investors,
- developed a podcast together with fellow students from my graduate programme ([see www.drops-of-hope.com](http://drops-of-hope.com)),
- wrote a thesis on Design Thinking at the intersection of innovation management and design studies (titled *"Design Thinking in Theory and Practice"*)
- worked as Policy Analyst trainee in a Ministry for Digital Affairs, delving into regional digital transformation strategies,

Previously, I was Entrepreneur in Residence at a financial technology startup, managing a high growth phase after venture capital backing, creating financial models and implementing GDPR compliance; I helped a sustainability bank to start wholly new, digital product and learned the intracacies of digital health at a health technology startup. More recently, I was  Policy Analyst trainee at a Ministry for Digital Affairs, delving into regional digital transformation strategies.

I am a fellow of [German National Academic Foundation](https://www.studienstiftung.de/en/) and graduated in Philosophy & Economics from the University of Bayreuth, with research semesters at the University of British Columbia in Vancouver, Canada, and the Free University of Bolzano, Italy.

I am dearly in love with Italian espresso, enjoy reading the Financial Times, and learning new technical things such as [Hugo](https://gohugo.io/).