---
date: "2021-03-18"
title: "Imprint"
layout: single
---

### (German version below)

Responsible according to §5 TMG (Telemediengesetz)


`Matthias Meller`\
Emmeransstrasse 6\
55116 Mainz\
E-Mail: *matt[dot]meller[at]me[dot]com*

Editor responsible for the content of this website in accordance with § 55 Abs. 2 RStV (Rundfunkstaatsvertrag, Interstate Broadcasting Agreement):\
Matthias Meller\
postal address and contact as above

*Please note that in doubt only the German version (see below) of the Imprint applies.*

***

### Impressum

Verantwortlich gemäß §5 Telemediengesetz (TMG):

Matthias Meller\
Emmeransstrasse 6\
55116 Mainz\
E-Mail: *matt[dot]meller[at]me[dot]com*

Redaktionell verantwortlich für die Inhalte dieser Webseite im Sinne des Presserechts gemäß § 55 Abs. 2 RStV:\
Matthias Meller\
Anschrift und Kontakt wie oben

#### Haftung für Inhalte

Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zuforschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.

#### Haftung für Links

Unser Angebot enthält Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.

#### Urheberrecht

Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb derG renzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritterbeachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eineUrheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.

*Quelle: eRecht24.de, [Jens Lechtenbörger](https://lechten.gitlab.io/imprint.html)*
