---
title: "Master Research Repository"
date: "2021-10-24"
layout: single
---

This page serves as a repository to my research for my master thesis on the credibility contest between members of the Scientific Advisory Group in Emergencies (SAGE) and members of Independent SAGE, with a focus on the activities of individuals on Twitter.

**Study time period**: 1 January 2020 to 30 June 2021.

***

**Table of Contents**

{{< table_of_contents >}}

***

## 0. Research Questions

This research is guided by the interest to understand the contestation of UK's government-sanctioned science advisory mechanism, Scientific Advisory Group in Emergencies (SAGE), in the Coronavirus pandemic through the emergence of the unofficial group of scientists called "Independent SAGE". This "source of contrarian expertise" (Jasanoff, Hilgartner, Hurlbut et al., 2021, p. 19) has early on called for a more robust, consistent public health response and greater transparency how, which and whose scientific advice fed into UK's emergency policymaking. This research interest thus touches on the themes of epistemic authority, credibility of science advice, and the boundary / interface between science and policy.

Twitter is increasingly recognised as an important site for science communication and the negotiation of expertise in Science and Technology Studies (STS). It has also frequently and prominently been used during the pandemic, particularly among academics, to share the latest thoughts, analysis, evidence or even policy advice. Against this backdrop, my primary research question is, *How have SAGE members, confronted with contestations of SAGE's credibility, communicated on Twitter in the period between January 2020 and June 2021 in contrast to members of Independent SAGE?*

**This research programme is operationalised through two secondary questions:**

1. When and with regard to which themes has the communcation of SAGE members on Twitter differed to those of Independent SAGE?
2. How have SAGE members drawn the boundary between science and policy in contrast to members of Independent SAGE?

***

## 1. Descriptive-quantitative Analysis

### 1.1. Basic numbers

|   Group   |   No. of individuals    |    w/ Twitter account   |
|  ---  |  ---  |  ---  |
|    SAGE experts   |    142   |    77   |
|    SAGE observers   |    32<sup>1</sup>   |    12   |
|    Independent SAGE   |   22<sup>2</sup>   |    20   |

<small><sup>1)</sup> Individuals were categorised as "SAGE observers" when they were filed as "Observer" in the official minutes of SAGE meetings and attended at least three SAGE meetings up until SAGE meeting no. 89.

<sup>2)</sup> I've included the "official" Independent SAGE Twitter account in this number.
</small>

### 1.2. The Twitter corpus

|   Group    |   Tweets in period<sup>3</sup>    |   Retweets only    |    Total    |
|  ---  |  ---:  |  ---:  |  ---:  |
|   SAGE experts    |   64,872    |   48,648    |    113,520   |
|   SAGE observers    |    1,856   |    1,594   |    3,450   |
|   Independent SAGE    |    45,817   |   43,602   |    89,419   |
|  SUM  |  **112,545**  |  **93,844**  |  **206,389**  |

<small><sup>3)</sup> Tweets in period exclude retweets.</small>

### 1.3. Analysing tweet volume

Twitter API reference: [v2 Tweet count](https://developer.twitter.com/en/docs/twitter-api/tweets/counts/introduction)

#### 1.3.1. Interactive point diagrams

Visually explore the number of daily tweets of the studied individuals across the selected study period. Due to the high number of Twitter accounts among them, the SAGE experts were excluded from this visual analsis.

- [Independent SAGE](/files/2021-10-21_indieSAGE_tweet-count_timebuckets.html)
- [SAGE observers](/files/2021-10-21_SAGE_observers_tweet-count_timebuckets.html)

#### 1.3.2. Interactive heatmaps

Visually explore the number of daily tweets for all groups with these interactive heatmaps (colour-coded, with tooltip to show the number of tweets of a single day). Quantitative basis are number of tweets per each studied individual, segmented by day.

- [SAGE experts](/files/2021-10-22_SAGE_experts_tweet-count_timebuckets_heatmap.html)
- [SAGE observers](/files/2021-10-22_SAGE_observers_tweet-count_timebuckets_heatmap.html)
- [Independent SAGE](/files/2021-10-22_indieSAGE_tweet-count_timebuckets_heatmap.html)

### 1.4. Historical analysis of follower count and changes in profile descriptions

The Twitter API does not allow a "historical" analysis of developments on the platform beyond exploring tweets with the [full-archive search endpoint](https://developer.twitter.com/en/docs/twitter-api/tweets/search/quick-start/full-archive-search). This endpoint gives insights into e.g., the individual engagement metrics such as number of retweets or quotes of a single tweet. However, it does not allow to explore these aspects in the specific context of its author at the time of publication—such as the number of followers. 

An approximation, albeit an inconsistent one, provides the database of the Internet Archive's [Wayback Machine](https://web.archive.org/) with its snapshots of websites from the past. I've queried the Wayback Machine for snapshots of the Twitter profiles of the individuals of interest to paint a picture of development of follower counts and changes in profile descriptions over time. The queries cover the period between 15 December 2019 until 15 July 2021. I've included the latest datapoint from the Twitter API (name, follower count, profile description) for each individual as consistent point of comparison.

- [SAGE experts](/files/2021-10-27_SAGE_experts_historical-development_followers-bio.html)
- [Independent SAGE](/files/2021-10-26_indieSAGE_historical-development_followers-bio.html)
- [Logarithmic chart to compare growth, SAGE experts](/files/2021-10-28_SAGE_experts_historical-development_log_followers-bio.html)
- [Logarithmic chart to compare growth, Indendent SAGE](/files/2021-10-28_indieSAGE_historical-development_log_followers-bio.html)

**Several observations can be made from this data**:

- Contrary to my hypothesis that profile descriptions ("bio") of the studied individuals may have changed throughout time to signal / broadcast / reflect a different public stance, such as marking membership to either SAGE or Independent SAGE, changes were less markedly.
- Christina Pagel for instance, one of the most prolific members of Independent SAGE, did not mark her membership in her profile description before early December 2020. The first snapshot in the Wayback Machine data that includes this is from early February 2021.
- The first profile description of Independent SAGE's official (or collective) account is a lot more bleak to today's version of *An independent group of scientists providing transparent advice during the COVID crisis*. In the snapshot from 14 June 2020 the profile description reads: **An independent group of scientists providing transparent advice with the purpose of helping the UK navigate COVID-19 whilst minimising fatalities**
- The three top accounts of **Independent SAGE** are the official @IndepedentSAGE (over 160,000 followers), Christina Pagel's (over 145,000 followers, from less than 10,000 in August 2020) and Anthony Costello's (over 80,000 followers, from about 8,000 followers in early January 2020).
- The three top accounts by followers among **SAGE members** are Chris Whitty (over 350,000 followers, from about 5,000 followers at the start of 2020), Patrick Vallance (~185,000 followers, from ~25,000 in January 2020) and Muge Cevik (~150,000 followers, from ~12,000 in March 2020)

### 1.5. Comparing the Top 25 tweets from SAGE experts and Independent SAGE

Preempting a full automatic text analysis of the entire downloaded Twitter corpus, I've drafted a small analysis based on the Top 25 tweets from SAGE experts and Independent SAGE, as measured by the retweet count and the quote count (i.e., a tweet consisting of a comment preceeding a retweet). The data stems from Twitter's [full-archive search endpoint](https://developer.twitter.com/en/docs/twitter-api/tweets/search/quick-start/full-archive-search).

Differences between Independent SAGE and SAGE experts in this arguably tiny sample can be spotted easily: Independent SAGE members criticize goverment policy, SAGE members focus more on contextualising evidence on COVID-19.

[Comparison Top 25 tweets, SAGE experts and Independent SAGE](/files/2021-10-31_comparison_top25_tweets_SAGE-experts_IndieSAGE.html)

### 1.6. Comparing the Top 40 tweets from SAGE experts and Independent SAGE

Widening the example to compare the 40 most retweeted tweets from members of both groups yields a clearer picture of the salient themes.

[Comparison Top 40 tweets, SAGE experts and Independent SAGE](/files/2021-12-03_comparison_top40_tweets_SAGE-experts_IndieSAGE.html)

**Observations:**

- **Chris Whitty** engages in clear public-facing messaging, calling for prudence (e.g., social distancing) or commenting on new evidence. **Muge Cevik** (only one-time SAGE member) has the most retweeted tweets, attempting to communicate and explain the latest evidence. On 29 May, **Jeremy Farrar** calls for not lifting the lockdown in England:

    *Covid-19 spreading too fast to lift lockdown in England. Agree with John &amp; clear science advice. TTI has to be in place, fully working, capable dealing any surge immediately, locally responsive, rapid results &amp; infection rates have to be lower. And trusted https://t.co/ZmYKs4Go3W*

- **Stephen Reicher**, member of SAGE sub-group SPI-B, and Independent SAGE member lands the most retweeted tweet of all on May 24, 2020, with a harsh critique of Boris Johnson, (followed by two further highly retweeted tweets in the same vein on the same day):

    *As one of those involved in SPI-B, the Government advisory group on behavioural science, I can say that in a few short minutes tonight, Boris Johnson has trashed all the advice we have given on how to build trust and secure adherence to the measures necessary to control COVID-19.*

- **Anthony Costello** repeatedly heavily criticises government policy across 2020, including the herd immunity strategy (on 13 March 2020), the UK's test and trace system, and calling for then Health secretary Matt Hancock to resign (19 June 2021): 

    *Time for Hancock to resign. No data on speed of testing, call centers dont work, app doesn't work, no data on isolation, wasted taxpayers money, economy at risk, public health staff doing all the work without investment.*

- With the start of 2021, however, **Christina Pagel** takes the reign of the government critique, such as commenting on the deteriorating pandemic situation in England in early January ("Covid-19 in England. This are going from very bad to much worse. […]") to taking a direct aim at PM Boris Johnson:

    *THREAD: Boris Johnson is basically talking bollocks about the 3rd wave in Europe inevitably coming here and he must know it.It's cynical opportunism to blame any resurgence in England on the European wave. It is frankly absurd. Here's why: 1/6https://t.co/GxmfZKdhFZ*

### 1.7. Comparing the Top 40 tweets from SAGE / Independent SAGE before 29 May 2020

Three aspects presented challenges to SAGE in May 2020: the emergence of Independent SAGE on May 4, the release of the names of SAGE members on the same day (first leaked by the Guardian about a week earlier) and the publication of its minutes with the beginning of 29 May. This time period is thus particularly interesting to compare the most retweeted tweets.

[Comparison Top 40 tweets before 29 May, SAGE experts and Independent SAGE](/files/2021-12-03_comparison_top40_tweets_SAGE-experts_IndieSAGE_before-May29.html)

**Observations:**

- On 14 March 2020, two days before the publication of the "Ferguson model", **Adam Kucharski** (London School of Tropical Medicine & Hygiene, SPI-M member, twice on SAGE later) notes his concern about "the message that the UK is actively pursuing 'herd immunity'" as policy strategy (retweeted ~6000 times).
- In the following weeks, **Chris Whitty** and **Neil Ferguson** top the most retweeted tweets, with calls for prudence and vigilance, respectively attempts to explain his pandemic modelling.

    *I’m conscious that lots of people would like to see and run the pandemic simulation code we are using to model control measures against COVID-19. To explain the background - I wrote the code (thousands of lines of undocumented C) 13+ years ago to model flu pandemics...*

- Among the later messages, **Cath Noakes**'s tweet from 24 May 2020 stand out, actively distancing herself from UK Government while acknowledging her SAGE membership.

    *As a SAGE member I try to give impartial advice based on the best scientific evidence available to supposedly help government make appropriate decisions. It doesn’t mean that I in any way support or condone the actions of ministers or their advisors.*

- Among the most retweeted tweets from Independent SAGE members, over half of them come from **Anthony Costello** calling UK policy "reckless" (15 March 2020), criticising Deputy Chief Medical Officer Jenny Harries (19 April 2020) and directly calling SAGE for not being independent and not diverse enough in expertise.

    *SAGE: 13 out of 23 are paid government advisers so not independent when in a room with D Cummings. […]*

    *SAGE: 0 molecular virologist 0 ICU expert 0 nurse 0 epidemic control expert 0 social scientist 0 public health scientist 0 information technology scientist 0 citizen scientist 0 digital app innovator 0 logistician 0 adviser from Hong Kong, China or Korea 0 adviser from WHO (2)*

- **Sir David King**'s tweet from 3 May 2020 can understood to form the prelude to the emergence of Independent SAGE, and its relationship to scientists from SAGE.

    *Science is a discipline based on peer review, therefore it is critical scientific advice is transparent. We are convening this meeting not to criticise the scientists on SAGE but to ensure that what we believe is the best science is available and in the public domain. https://t.co/ZaZXJuI1TT*

- The most retweeted tweets comes from **Stephen Reicher** heavily criticising the PM and the UK Government. He expresses his hopes that the UK public will be receptive to science advice, directly coming from Independent SAGE.

    *It is very hard to provide scientific advice to a government which doesn't want to listen to science. I hope, however, that the public will read our papers (publicly available at https://t.co/TFK4vYbLiL) and continue to make up for this bad  government with their own good sense.*

- Generally, among members of Independent SAGE, the critique of UK Government and how SAGE is set up ranks high.

### 1.8. SAGE Core Group

Both Haddon, Sasse and Nice (2020) from the *Institute for Government* and the report "The UK response to covid-19: use of scientific advice" (2021) by the House of Commons *Science and Technology Committee* (STC) have noted "a lack of clarity between those experts who had formed the ‘core’ of SAGE participants" (p. 25). My analysis of SAGE meeting minutes shows great differences in total attendances. Many individuals, both experts or observers sent from UK departments or UK Government, have attended only a handful of meetings. This raises the question what being a member of SAGE actually entails.

Setting a `threshold of at least 10 SAGE meeting attendances`, according to the attendee lists included in the meeting minutes, I have analysed who of the individuals in my SAGE experts Twitter corpus belong to the "core group" and their incoming / outgoing date to the panel.

[SAGE Core Group – incoming / outgoing](/files/2021-12-03_SAGE_core_group_twitter_first-last-meeting.html)

***

## 2. Structural Topic Modelling

As second step of my research methodology I chose a flavour of automatic text analysis to support my analysis of the Twitter corpus, called [Structural Topic Modelling](https://www.structuraltopicmodel.com/) (see [here](https://cbail.github.io/SICSS_Topic_Modeling.html) for a good explainer). Topic modelling is a statistical approach to analyse large quantities of text, based on the assumption words (co-)occur in groups ("bags-of-words"), and the appearance of groups in texts confer meaning and context to them. While topic modelling estimates which themes ("topics") a corpus of documents entails, the structural topic modelling takes this approach further: it allows to inspect topics in relation to the metadata of the analysed documents, e.g. publication date or affiliation of the author.

Structural Topic Modelling has proved to be a useful methodology in social science research, also for the study of platforms as Twitter. In ["May it please the twitterverse: The use of Twitter by state high court judges"](https://www.tandfonline.com/doi/full/10.1080/19331681.2019.1657048) Curry and Fix (2019) analysed the changes in use of Twitter by US state high court judges, important representatives of the state traditionally understudied, when their election was on the horizon.

Structural Topic Modelling (stm) is based on an unsupervised machine learning algorithm, available for deployment through a package for `R` [see Roberts, Stewart & Tingley, 2019](https://raw.githubusercontent.com/bstewart/stm/master/vignettes/stmVignette.pdf).

### 2.1. A first run

#### 2.1.1. Pre-Processing

After manual inspection and a test run with the `stm` package, the Twitter data of the SAGE Observers was discarded from the analysis.

The Twitter data of both SAGE and Independent SAGE members was read into `R`, comprising of tweet ID, tweet text, author ID, publication date/time, public metrics (retweet, reply, like, quote count). Before both corpura were merged, a variable was added to later identity the category of the author for each document (tweet), either being "SAGE" or "Independent SAGE". This is relevant for later analysis. The corpus was then tokenised, removing URLs, symbols (including all? emojis), a standard set of English stopwords, a further custom set of stopwords, and all one-character tokens expect "r". Furthermore, all tokens were converted to lowercase while still preserving accronyms (identified as min. 2 capitalised characters). 

*Important*: the tokens have neither been stemmed or lemmatised. In a test run on the SAGE observer corpus, stemming with the standard procedure by the `quanteda` package proved to be not useful (e.g., "important" was stemmed to "import"). Also, any abbreviations such as "gov't" for "government" will likely produce tokens not suited for meaningful analysis. Lemmatisation, however, is a more complex procedure which I have not yet conducted due to time constraints.

Before handing over the resulting matrix of documents (id + list of tokens) plus metadata ("docvars") to `stm`, I removed any document with less than 4 tokens, which reduced the number of documents of ~101,000 by almost 7,800. Furthermore, I've removed all tokens from the corpus that were present in less than 15 documents overall. This resulted in dropping about 89% of all tokens, warranted for a reasonable computation time of the model.

#### 2.1.2. Choosing the optimal number of topics

Working with (structural) topic modelling poses the problem of choosing the optimal number of topics for a given corpus. While `stm` estimates topics automatically, it relies on a user-specified number of topics *K*. I chose the data-driven approach to selecting the optimal number of topics via the `searchK` function that calculates four diagnostic values for stm models at a given range of *K*. 

- *Held-Out Likelihood*: Estimates the likelihood that words appear within a document of the corpus, despite being removed from the document in the model estimation function. Is maximised at 0, because that means words do **not** appear within a document while not being present in the model estimation. (See Roberts et al. 2019, p. 38)
- *Residuals*: Performs a residual analysis, testing whether the variance between the observed and the data estimated by the stm model can be sufficiently explained by the given number of topics *K*. If the residuals score is high, it could be that more topics are needed to "soak up some of the extra variance" (Roberts et al. 2019, p. 38).
- *Semantic Coherence*: see below (`section 2.1.3.`)
- *Lower Bound*: [to be added]

##### *Running algorithm for K = 20 / 50 / 75*

[![Running algorithm for K = 20 / 50 / 75](/files/search-optimal-k-20-50-75-first-model.png "Click to enlarge")](/files/search-optimal-k-20-50-75-first-model.png)

##### *Running algorithm again for K = 10 / 15 / 20 / 25 / 30 / 40 / 50*

Based on my evaluation of an optimal variation across held-out likelihood, semantic coherence and residuals, I chose **K = 25** as optimal number of topics for the present estimation, with topic prevalence estimated as `prevalence =~ author_cat + s(date_numeric)`.

[![Running it again for K = 10 to 50, optimal at K = 25](/files/search-optimal-k-10-15-20-25-30-40-50-first-model.png "Click to enlarge")](/files/search-optimal-k-10-15-20-25-30-40-50-first-model.png)

#### 2.1.3. Exploring stm at K = 25

Plotting the topics of the estimated model, ranked by their expected proportions across the corpus with the `plot type = "summary"` function. The words following each topic name are the top three words associated with this topic. I have higlighted the topics later selected for their interpretative quality with a red line.

[![Top Topics by expected probability](/files/2021-11-11-second-model-topic-proportions.png "Click to enlarge")](/files/2021-11-11-second-model-topic-proportions.png)

The `topicQuality` function allows to explore each topic's variation in the parameters *semantic coherence* and *exclusivity* for a given model. **Semantic coherence** could be explained as an 'average interpretability' of a topic, because the most probable terms statistically speak for the topic. As a metric, semantic coherence is maximised when "the most probable words in a given topic frequently co-occur together" (Roberts et al., 2019, p. 10). According to [Mimno, Wallach, Talley, Leenders, and McCallum (2011)](https://dl-acm.org/doi/10.5555/2145432.2145462), high scores of a topic in semantic coherence correlates well with human judgement of its interpretability. Measuring **exclusivity** is based on the FRequency-EXclusivity (FREX) score as detailed below (`section 2.1.4.`).

I have circled my selection of top topics by semantic / exclusivity in red.

[![Exploring topic quality by semantic coherence / exclusivity](/files/2021-11-11-second-model-topic-quality.png "Click to enlarge")](/files/2021-11-11-second-model-topic-quality.png)

#### 2.1.4. Topics with best semantic fit

The topics `12, 13, 14, 7, 10, 16, 9 and 19`, selected for their relatively high scores on semantic coherence and exclusivity, are shown here with their top words in four categories, thanks to the `labelTopics` function:

- *Highest Prob*: words with the highest probability to be associated with this topic
- *FREX*: weighting words by frequency and exclusivity to specific topic, while giving greater weight to exclusivity. Balances out very frequent yet common terms in favour of words that significantly co-occur more often only with this topic.
- *Lift*: Divides frequency of words in this topic by frequency in other topics → greater weight to words appearing less frequently in other topics.
- *Score*: Diving log frequency of words in this topic by log frequency in other topics.

[![Top words](/files/2021-11-11-semantic-fit-topics-top-words.png "Click to enlarge")](/files/2021-11-11-semantic-fit-topics-top-words.png)

Prevalence of individual topics is estimated as `author category` (either SAGE or Independent SAGE member; linear independent variable) plus `day` as non-linear covariate (between 1 Jan 2020 and 30 June 2020; on a b-spline basis, same approach as in [Roberts et al. 2019](https://raw.githubusercontent.com/bstewart/stm/master/vignettes/stmVignette.pdf)).

[![Prevalence of topics over time](/files/2021-11-11-second-model-topic-prevalance-time.png "Click to enlarge")](/files/2021-11-11-second-model-topic-prevalance-time.png)

### 2.2. A new approach to pre-processing

In the first run I have limited efforts of pre-processing mostly to removing a collection of stopwords and one-character tokens and trimming the document-feature matrix, removing any document with less than 4 tokens overall and all tokens that were present in less than 15 documents. I did not pursue stemming, intuitively apparently for a good reason (see [Schofield & Mimno, 2016](https://transacl.org/ojs/index.php/tacl/article/view/868)). 

For the second run at modelling the corpus, I opted to use a lemmatiser to make the document-feature matrix as input for `stm` more meaningful. Lemmatisation is a good practice in natural language processing (NLP) to make texts easier to analyse for algorithms. In contrast to the relatively 'brute force' stemming, cutting away characters up to a 'root', a lemmatiser converts a word into its base form while considering the context (i.e., which 'part of speech' it forms). Ideally, a lemmatiser properly identifies that the base form of "caring" is "care" and not "car", or that "us" is a derivative of "we" and not the abbreviation for the United States.

I chose [spaCy](https://spacy.io/) ("Industrial-Strength Language Processing") for which the authors of `quanteda` developed a wrapper package in R, called `spacyR`. In order to get `spacyR` running, however, I had to invest a lot more thoroughly in pre-processing of my data. This included removing mentions, URLs and hashtags from the reference dataset. Also, after heavily studying the corpus with the `kwic()` function, I identified common abbreviations ("govt" for "government" or abbreviated names of months) and replaced them accordingly. To increase the meaningfulness of the tokens, I also created compound tokens (such as "science_advice" or "intensive_care") and bi-grams / tri-grams (token sequences of originally two or three single tokens, e.g. "test_trace_isolate"). Thanks to `spacyR`'s part of speech ("POS") recognition I trimmed down the lemmatised tokens to nouns, adjectives, adverbs and verbs only.

This wordcloud shows the Top 150 terms ("features") by frequency across all documents, including bi-grams (see "public_health").

[![Wordcloud dfm_spacied top150](/files/master-research/pre-processing/2021-12-01_dfm_spacied_combined_wordcloud_top150.png "Click to enlarge")](/files/master-research/pre-processing/2021-12-01_dfm_spacied_combined_wordcloud_top150.png)

Having previously extracted and saved mentions and hashtags to separate dfm's, I've also created wordclouds and network plots for them.

- [Hashtags Top 100 Wordcloud](files/master-research/pre-processing/2021-12-01_hashtags_dfm_wordcloud_top100.png)
- [Mentions Top 100 Wordcloud](/files/master-research/pre-processing/2021-12-01_mentions_dfm_wordcloud_top100.png)
- [Hashtags Network Plot (Top 100)](/files/master-research/pre-processing/2021-12-01_hashtags_networkplot_top100.png)
- [Mentions Network Plot (Top 75)](/files/master-research/pre-processing/2021-12-01_mentions_fcm_wordcloud_top75.png)

In the following structural topic modelling 87448 documents (i.e., tweets) and 6800 terms words were considered, creating a 87448*6800 document-feature matrix (594.6m entries) to estimate topics models from.

### 2.3. A new (linear) prevalence model

With the "finetuned" data I estimated a new topic model, again with two covariates I believed to influence the frequency in which topics were discussed; first, the group membership of the author of a tweet ("document"), either being SAGE or Independent SAGE, and second, the day of publication. I specified the estimation of topical prevalence with the formula `prevalence =~ author_cat + s(date_numeric)`, where `author_cat` represents the author category and `s(date_numeric)` is the numeric transformation of the publication day, entered as non-linear covariate, smoothed by b-spline. Splines are approximation procedures for polynomial functions. b-spline or *basis-spline* has ten degrees of freedom, which means for the regression estimatation of topic proportions estimated by the `estimateEffect()` function, the non-linear covariate *date_numeric* enters with 10 partial polinomial terms.

Topic modelling presents the analyst with an initalisation problem: prior to, figuratively speaking, "set free" the unsupervised learning algorithm, the numbers of clusters, i.e. topics, needs to be pre-determined. This easily presents "one of the most difficult questions" (Grimmer and Stewart, 2013, p. 19) in the process and is heavily dependent on the model / corpus. Choosing (rather then discovering) the "true" number of topics is highly impactful and the bottleneck of every topic modelling. With neither time nor computing power being at infinite supply that choice almost certainly involves a trade-off. In the manual to the stm package, the authors [Roberts et al. (2020)](https://cran.r-project.org/web/packages/stm/stm.pdf) write:

```
The most important user input in parametric topic models is the number of topics. There is no right answer to the appropriate number of topics. More topics will give more fine-grained representations of the data at the potential cost of being less precisely estimated. […] For short corpora focused on very specific subject matter (such as survey experiments) 3-10 topics is a useful starting range. For small corpora (a few hundred to a few thousand) 5-50 topics is a good place to start. Beyond these rough guidelines it is application specific. Previous applications in political science with medium sized corpora (10k to 100k documents) have found 60-100 topics to work well. For larger corpora 100 topics is a useful default size. Of course, your mileage may vary.
```

#### 2.3.1. Linear prevalence model at K = 20

With the data-driven approach of the `searchK()` function to finding the optimal number of topics to estimate a model of the document corpus, I settled on K = 20 topics, after an initial runs of `searchK()` for K = 20/40/60/80 and K = 5/10/15/20/25/30/35 yielded either 15 or 20 to be the optimal number of topics. In the selection process I optimised for held-out likelihood and semantic coherence, followed by residuals, thus opted to run a topic model with K = 20 topics, given the notably superior diagnostic values for semantic coherence at K = 20 over K = 40 and relatively less pronounced worse values for held-out likelihood / residuals. Despite K = 15 scoring notably better on semantic coherence, I selected K = 20 as I judged 15 topics too few to account for the large document corpus.

[![searchK() - Diagnostic values for K = 20/40/60/80](/files/master-research/model-prevalence/2021-12-02_searchK_stm_prev_20-40-60-80.png "searchK() - Diagnostic values for K = 20/40/60/80")](/files/master-research/model-prevalence/2021-12-02_searchK_stm_prev_20-40-60-80.png "Click to enlarge")

[![searchK() - Diagnostic values for K = 5 to 35](/files/master-research/model-prevalence/2021-12-02_searchK_stm_prev_5-10-15-20-25-30-35.png "searchK() - Diagnostic values for K = 5 to 35")](/files/master-research/model-prevalence/2021-12-02_searchK_stm_prev_5-10-15-20-25-30-35.png "Click to enlarge")

After close reading of at least 50 to 100 example documents (ranked by their estimated probability to be strongly associated) for each topic, I named each topic. The following graph shows the expected proportion that each topic takes from the entire document corpus.

[![Topic proportions](/files/master-research/model-prevalence/2022-01-10_model-prevalence_k20_top-topics.png "Topic proportions")](/files/master-research/model-prevalence/2022-01-10_model-prevalence_k20_top-topics.png "Click to enlarge")

With the `summary()` function the `stm` package generates a summary of the highest associated words to all topics, segmented by four metrics:

- *Highest Prob*: words with the highest probability to be associated with this topic
- *FREX*: weighting words by frequency and exclusivity to specific topic, while giving greater weight to exclusivity. Balances out very frequent yet common terms in favour of words that significantly co-occur more often only with this topic.
- *Lift*: Divides frequency of words in this topic by frequency in other topics → greater weight to words appearing less frequently in other topics.
- *Score*: Diving log frequency of words in this topic by log frequency in other topics.

The following plot shows the most associated words for each topic, by probability ("raw probabilities") and frequency-exclusivity ("FREX") weighting.

[![Top words by probability / FREX](/files/master-research/model-prevalence/2022-01-10_model-prevalence_topic-labels_prob-frex.png "Top words by probability / FREX")](/files/master-research/model-prevalence/2022-01-10_model-prevalence_topic-labels_prob-frex.png "Click to enlarge")

The close reading of the 20 topics in conjunction with the analysis provided by the `topicQuality()` function, which evaluates the quality of topics in a given model by their semantic coherence and exclusivity, yielded 12 topics of interest that scored high on a combination of both metrics (excluding for Topic 1, "Appreciation for data, articles", Topic 3, "New year's/birthday greetings", and Topic 4, "Recruiting researchers" since they were not of semantic interest).

[![Topic Quality](/files/master-research/model-prevalence/2022-01-10_model-prevalence_k20_topicQuality.png "Topic Quality")](/files/master-research/model-prevalence/2022-01-10_model-prevalence_k20_topicQuality.png "Click to enlarge")

For the 12 selected topic interest, the topic prevalence over time was estimated.

[![Topic Prevalence over time](/files/master-research/model-prevalence/2022-01-07_model-prevalence_estimateEffect.png "Topic Prevalence over time")](/files/master-research/model-prevalence/2022-01-07_model-prevalence_estimateEffect.png "Click to enlarge")

**Observations and comments:**

- Three observations surface from the estimation of topic prevalence over time immediately: **First**, for all selected topics the date of publication plays an important role as all curves exhibit notable slopes. The slopes at the beginning / towards the end of the curves should, however, be taken with a grain of salt, on the one hand as already indicated by the significantly larger 95% confidence interval (CI) area (enclosed by the dashed line), but also because of Ted Underwood's (2018) observation that topic models may easily a bit "curved" towards each ends (see [here](https://tedunderwood.com/2018/07/26/do-topic-models-warp-time/)). **Second**, in most topics (ten out of twelve) the linear covariate authorship, i.e. a tweet either being from a SAGE or Independent SAGE member, does make notable difference for the model estimation. Only in two topics (#10, #18) both curves are reasonably close. **Third**, in seven out of twelve topics (#5, #7, #9, #11, #13, #14, #15) members of Independent SAGE lead the conversation, while only in three (#8, #16, #17) members of SAGE have the heft in hand. With regard to this observation, one has to note that the reason, why both curves for each topic run in parallel, lies in the estimation formula `prevalence =~ author_cat + s(date_numeric)`. Both covariate enter the estimation as linear factors, thus it follows logically that under this estimation the factor authorship can only shift the curve in the vertical dimension. An estimation at what point during the study period both groups diverged is not possible in this model.
- While the topic modelling has not yet been validated according to [Grimmer and Stewart's (2013)](https://www.cambridge.org/core/product/identifier/S1047198700013401/type/journal_article) fourth principle of quantitative text analyis "Validate, Validate, Validate" (see p. 5) beyond labelling, this process already demonstrated the limited usefulness of the modelling. As captured in the label for **Topic 15**, its highest ranked examples are a mix of considerations of social psychology and the pandemic, interspersed or interlinked with documents on the Black Lives Matters, respectively the Hong Kong protests. **Topic 8** on the other hand encapsulated discussions on the importance of indoor ventilation, while also included tweets from Elizabeth Stokoe (@LizStokoe, Independent SAGE) informing her audience about the daily experience in nature at a high frequency. Also, and very relevant for the interest of this study, **Topic 9** broadly captured criticisms of the UK government, sometimes pandemic related when referring to Dominic Cummings, but much more conflated by a reincarnation of the Windrush scandal with the release of the "Windrush report" by UK's Home Office ([Gentleman and Bowcott, 2020, *The Guardian*](https://www.theguardian.com/uk-news/2020/mar/19/windrush-report-condemns-home-office-ignorance-and-thoughtlessness)).

#### 2.3.2. Linear Prevalence model at K = 35 / 40 / 45

Given the previous considerations that in the K = 20 prevalence model single topics soak up to many different thematic strands, I've re-evaluated my model selection, which started off from optimising held-out likelihood and semantic coherence at equal measure. Following best practices (including [Julia Silge](https://juliasilge.com/blog/evaluating-stm/), Ryan Wesslen ([a](https://github.com/wesslen/text-analysis-org-science)/[b](https://github.com/wesslen/NCStateSenateFacebook)), [Julian Unkel](https://bookdown.org/joone/ComputationalMethods/topicmodeling.html) and tips by the package's authors, see Roberts et al., 2019, p. 38), I now first optimised for residuals, then held-out likelihood, followed by semantic coherence.

As demonstrated by the first run of `searchK()` for K = 10/20/30/40/50/60/70/80/100, residuals approach a trough around K = 40. Held-out likehood is maximised at K = 100 (~-7.36), however, semantic coherence at K = 100 is lowest/worst (<-220). Semantic coherence hovers close to -180 for K = 40, with held-out likelihood standing at -7.42, which appears only slightly, i.e. in the small dimension of second-order decimals, worse than at K = 100.

[![Running model selection diagnostic for K = 10/20/30/40/50/60/70/80/100](/files/master-research/model-prevalence/2022-01-11_model-prevalence_searchK_k10-20-30-40-50-60-70-80-100.png "Running model selection diagnostic for K = 10/20/30/40/50/60/70/80/100")](/files/master-research/model-prevalence/2022-01-11_model-prevalence_searchK_k10-20-30-40-50-60-70-80-100.png "Click to enlarge")

Against this reasoning, I conducted another `searchK()` run for K = 30/35/40/45/50. This yielded the best (i.e., lowest) value for residuals at K = 35 (~12.8), about .7 and .4 better than for K = 40 respectively at K = 45. Held-out likelihood appeared to be close within narrow bounds for K = 35/40/45, while semantic coherence stood notably highest for K = 35 (~-185), most pronounced in comparison to K = 45 (~-200). It is helpful to consider that topic models with a smaller number of topics usually fare better in overall semantic coherence, as it is maximised, e.g. "if you only have a few topics dominated by very common words" as [Julia Silge (2018)](https://juliasilge.com/blog/evaluating-stm/) put it. 

[![Running model selection diagnostic again for K = 30/35/40/45/50](/files/master-research/model-prevalence/2022-01-11_model-prevalence_searchK_k30-35-40-45-50.png "Running model selection diagnostic again for K = 30/35/40/45/50")](/files/master-research/model-prevalence/2022-01-11_model-prevalence_searchK_k30-35-40-45-50.png "Click to enlarge")

Under this scenario, a comparison how different topic models at K = 35/40/45 fare in terms of the combination of semantic coherence/exclusivity (see section 2.1.4) appears warranted. For this purpose I ran the `selectModel()` function<sup>4</sup> of the `stm` package which proposes a number of high likelihood models for a given K and calculates their semantic coherence and exclusivity. All three runs of `selectModel()` provided me with four models with differently dispersed values for semantic coherence / exclusivity for their modelled topics. To compare the quality of models across all three batches, I calculated and ploted the mean semantic coherence / exclusivity - combination. The following graph exhibits a "semantic coherence/exclusivity frontier", on which the models with the best combination of both values lie, visually striving to the most upper right part of the plot.

[![Comparing semantic coherence/exclusivity – combinations for 12 models at K = 35/40/45](/files/master-research/model-prevalence/2022-01-12_model-prevalence_semcoh-excl-plot_comparison.png "Comparing semantic coherence/exclusivity – combinations for 12 models at K = 35/40/45")](/files/master-research/model-prevalence/2022-01-12_model-prevalence_semcoh-excl-plot_comparison.png "Click to enlarge")

<small><sup>4)</sup> The formula used was: selectModel*(out$documents, out$vocab, K = [35/40/45], prevalence = ~author_cat + s(date_numeric), data = out$meta, seed, runs = 20, max.em.its = 100, verbose = TRUE)</small>

The frontier in this plot again underscores the trade-off the analyst has to make while choosing the "true" number of topics. The best model at K = 45 fares better in exclusivity on average (9.926451; even though only in the dimension of second-order decimals), while the best model at K = 35 features a mean semantic coherence of -178.9191. Running another test to compare the entire topic quality across the three models on said frontier, however, allows us to dilute the trade-off from before a bit. When drawing a baseline of semantic coherence of at least -200 and exclusivity of 9.9 at minimum, the model at K = 45 features the highest number of purportedly 'good quality' topics, both in absolute and relative terms (K = 35: 14; K = 40: 21; K = 45: 25). Consequently, for the improved prevalence modelling **I select the model with K = 45**.

[![Comparing the topic quality of the three best models at K = 35/40/45](/files/master-research/model-prevalence/2022-01-12_model-prevalence_comparison-topic-quality_selected-k35-40-45.png "Comparing the topic quality of the three best models at K = 35/40/45")](/files/master-research/model-prevalence/2022-01-12_model-prevalence_comparison-topic-quality_selected-k35-40-45.png "Click to enlarge")

The following collection shows the top words for each of the 45 topics by probability and FREX (click to enlarge). Prior to calculating this collection, I labelled each topic after close reading of the 50 highest associated documents for each topic.

[![Comparing the topic quality of the three best models at K = 35/40/45](/files/master-research/model-prevalence/2022-01-13_model-prevalence_k45_prob-excl_preview.png "Top words by probability / FREX for model at K = 45")](/files/master-research/model-prevalence/2022-01-13_model-prevalence_k45_prob-excl.png "Click to enlarge")

During the labelling process, which on the one hand serves as precursor to the model validation process but is also an essential step for the analyst to qualitatively assess the quality of the individual topics, I observed that a number of topics were hard to interpret due to being semantically "not stable", even though while the documents associated with the topics did feature terms appearing exclusively. This points to a methodological consideration: topics which also max out the statistical metric "exclusivity" are not necessarily more meaningful, when compared with topics with smaller exclusivity and similar semantic coherence. Interestingly, my observation was that several topics in the K = 45 model were hard to interpret in a different way than those in the previous modelling at K = 20. Instead of confounding two (or more) semantically coherent themes into one topic, the modelling at K = 45 estimated topics which could have been semantically coherent in a statistical sense but less so for me as human analyst and domain expert.

For the selection of interesting / relevant topic I combined my qualitative assessment of all topics as part of the labelling process with the statistical analysis of semantic coherence / exclusivity. The following plot foregrounds the 18 topics that have been selected on the basis of their quality.

[![Topic quality based on semantic coherence / exclusivity](/files/master-research/model-prevalence/2022-01-13_model-prevalence_k45_topicQuality.png "Topic quality based on semantic coherence / exclusivity")](/files/master-research/model-prevalence/2022-01-13_model-prevalence_k45_topicQuality.png "Click to enlarge")

I have run a linear regression for the selected 18 topics to estimate their prevalence over time, moderated by the group of authors, exactly in the same way as above. Note that, given the greater number of topics of the entire model the expected topic proportion for each topic in the estimation is, of course, smaller.

[![Estimation of topical prevalence over time for 18 selected topics](/files/master-research/model-prevalence/2022-01-13_model-prevalence_k45_estimateEffect.png "Estimation of topical prevalence over time for 18 selected topics")](/files/master-research/model-prevalence/2022-01-13_model-prevalence_k45_estimateEffect.png "Click to enlarge")

**Observation and comments:**

- In the modelling at K = 20, Topic 18 (*"Vaccine access, new variant, 'safe schools'"*) confounded three thematic streams that appear to have split into several distinct topics in this modelling; namely, Topic 1 (*"New virus variant(s), transmissions (growth)"*), and Topic 7 (*"'Safe schools' controversy"*). The imagined combination of the curves of Topic 1 and 7 would look close to the curve of the previously modelled Topic 18.
- Unsurprisingly, some themes have emerged in distinct topics which were not captured by topics previously, e.g. racial inequalities as pronounced risk factor for contracting COVID (see Topic 5, *"COVID risk factors and racial inequalities"*), the protection of care homes / critical care staff (see Topic 38) or the discussion around the lessons from lockdown(s) (Topic 16).
- The topics **for which time (`s(date_numeric)`) of has played a greater role**, as visually evidenced with spikes in the estimation curves are Topic 1 (*"New virus variant(s), transmissions (growth)"*), Topic 4 (*"Reforming Test, Trace, Isolate (TTI)"*), Topic 7 (*"'Safe schools' controversy"*), Topic 30 (*"Testing accuracy, mass testing controversy"*), Topic 38 (*"Protecting care homes, critical care staff"*), and, unsurprisingly, Topic 42 (*"Coronavirus mutation(s)"*).
- The topics with the **most pronounced divergence moderated by the covariate `authorship`** are Topic 3 (*"Discusing usefullness of models"*), Topic 4 (*"Reforming Test, Trace, Isolate (TTI)"*), Topic 7 (*"'Safe schools' controversy"*), Topic 8 (*"Importance of indoor ventilation due to airborne transmission"*), Topic 21 (*"Criticising UK's public health system"*), Topic 41 (*"Criticism of UK government, political leaders*") and Topic 42 (*"Coronavirus mutation(s)"*).
- Topics **with the least variability over time** are Topic 3 (*"Discusing usefullness of models"*), Topic 31 (*"Role of SAGE, (behavioural) scientists"*), Topic 32 (*"Vaccine roll-out in UK, delaying second dose?"*), and Topic 43 (*"Independent SAGE's weekly briefing"*).
- In this modelling, it is curious to see that the primary topic capturing criticisms of the UK government, Topic 41 (*"Criticism of UK government, political leaders*"), does not spike around Christmas 2020, as the comparable Topic 11 (*"Criticising UK government pandemic policy"*) from modelling K = 20 does so obviously.

### 2.4. A non-linear prevalence model

The previous models estimated topic prevalence with the linear formula `prevalence =~ author_cat + s(date_numeric)`. In the final step which evaluated topic prevalence over time, the collection of graphs show the consequence of this modelling approach: the covariate **time** (in the unit *day*, between Jan 1, 2020 and June 30, 2021) sets the baseline of the curve, with the covariate **authorship** modulating the entire height of the curve, i.e. the dimension of the expected topic probability across the entire time. What this model does not tell us, however, is how one author group may have effected the prevalence of a topic at a specific time. In essence, we are not provided with an estimation how different author groups have diverged on pushing a topic at a given time. Enter a new model to estimate the interactions between covariates, the **interaction model**.

Note, that this is a completely new model which can differ in topics, including their numbering compared the previous linear prevalence models. This is because the structural topic model needs to be fit for the covariates.

Running the `search()` function to narrow down the scope for the optimal number of topics for the interaction model first for K = 10 / 20 / 40 / 60 / 80 / 100, strongly suggested a value for K between 20 and 60. A re-run of `searchK()`for K = 20 / 25 / 30 / 35 / 40 / 45 / 50 / 55 / 60 suggested **K = 45** to be a good choice. 

[![Interaction model: searchK() for K = 10/20/40/60/80/100](/files/master-research/model-interaction/2022-01-14_model-interaction_searchK_10-20-40-60-80-100.png "Interaction model: searchK() for K = 10/20/40/60/80/100")](/files/master-research/model-interaction/2022-01-14_model-interaction_searchK_10-20-40-60-80-100.png "Click to enlarge")

[![Interaction model: searchK() for K = 20/25/30/35/40/45/50/55/60](/files/master-research/model-interaction/2022-01-14_model-interaction_searchK_20-25-30-35-40-45-50-55-60.png "Interaction model: searchK() for K = 20/25/30/35/40/45/50/55/60")](/files/master-research/model-interaction/2022-01-14_model-interaction_searchK_20-25-30-35-40-45-50-55-60.png "Click to enlarge")

Among the four high-likelihood models for K = 45, provided by the `selectModel()` function, model number 4 emerged as the one with the best semantic coherence / exclusivity. As the model hadn't converged yet during the run of `selectModel()` I re-estimated model #4 until convergence.

[![Selecting the best model for K = 45](/files/master-research/model-interaction/2022-01-14_model-interaction_selectModels_compare.png "Selecting the best model for K = 45")](/files/master-research/model-interaction/2022-01-14_model-interaction_selectModels_compare.png "Click to enlarge")

The labelling process, involving reading the top 50 - 100 documents associated with each topic, went smoother comparably to the previous linear prevalence model at K = 45. The following graph shows the expected proportions of the topics for the entire corpus. Each topic name is followed by the top 3 terms, ranked by probability (not FREX).

[![Expected topic proportions for the 45 topics of the interaction model](/files/master-research/model-interaction/2022-01-14_model-interaction_topic-proportions.png "Expected topic proportions for the 45 topics of the interaction model")](/files/master-research/model-interaction/2022-01-14_model-interaction_topic-proportions.png "Click to enlarge")

The top terms, ranked by probability and FREX, for all topics are exhibited in the following plot.

[![Top terms for all topics, ranked by probability / FREX](/files/master-research/model-interaction/2022-01-14_model-interaction_prob-frex-terms_preview.png "Top terms for all topics, ranked by probability / FREX")](/files/master-research/model-interaction/2022-01-14_model-interaction_prob-frex-terms.png "Click to enlarge")

Based on the combination of close reading of the top documents for each topic, and the statistical metrics proposed by Roberts et al. 2019, I've selected 21 topics to analyse more closely. As part of this selection process, I discarded topics that were either not relevant in terms of content to the nature of this study (e.g., Topic 13, *"Good wishes/family/celebrations [less relevant]"*) or hard(er) to interpret (e.g., Topic 22, *""Difficulty to answer questions [harder to interpret]"*). Furthermore, I chose to extend the range in which I considered semantic coherence / exclusivity high enough to -215 respectively 9.885 (shared area in orange). An exemption to this selection is Topic 20, *"COVID high risk factors, incl. diabetes, social and working conditions"*, for which I was particularly curious how the covariate authorship may influence the topic prevalence for a given point in time.

[![Selecting 21 interesting topics](/files/master-research/model-interaction/2022-01-14_model-interaction_topic-quality.png "Selecting 21 interesting topics")](/files/master-research/model-interaction/2022-01-14_model-interaction_topic-quality.png "Click to enlarge")

Estimating the interaction of the covariates author group and day for these 21 selected topics with `estimateEffect()` allows us to explore how the author group, red for Independent SAGE and blue of SAGE, is estimated to influence the prevalence of a given topic on a given point in time. Thus, in contrast to the previous linear prevalence modelling, the effect of the covariate author group on the prevalence of a topic at a given point in time is in the focus.

[![Estimating the interaction of author group and time for 21 selected topics](/files/master-research/model-interaction/2022-01-14_model-interaction_estimateEffect.png "Estimating the interaction of author group and time for 21 selected topics")](/files/master-research/model-interaction/2022-01-14_model-interaction_estimateEffect.png "Click to enlarge")

***

## References

##### *On Structural Topic Modelling*

- Banks, G.C., Woznyj, H.M., Wesslen, R.S., Ross, R.L., 2018. A Review of Best Practice Recommendations for Text Analysis in R (and a User-Friendly App). J Bus Psychol 33, 445–459. https://doi.org/10.1007/s10869-017-9528-3
- Chang, J., Boyd-Graber, J., Gerrish, S., Wang, C., Blei, D.M., 2009. Reading Tea Leaves: How Humans Interpret Topic Models 10. https://users.umiacs.umd.edu/~jbg/docs/nips2009-rtl.pdf
- Farrell, J., 2016. Corporate funding and ideological polarization about climate change. PNAS 113, 92–97. https://doi.org/10.1073/pnas.1509433112
- Grimmer, J., Stewart, B.M., 2013. Text as Data: The Promise and Pitfalls of Automatic Content Analysis Methods for Political Texts. Polit. anal. 21, 267–297. https://doi.org/10.1093/pan/mps028
- Mimno, D., Wallach, H.M., Talley, E., Leenders, M., McCallum, A., 2011. Optimizing semantic coherence in topic models, in: Proceedings of the Conference on Empirical Methods in Natural Language Processing, EMNLP ’11. Association for Computational Linguistics, USA, pp. 262–272. https://dl.acm.org/doi/pdf/10.5555/2145432.2145462
- **Roberts, M.E., Stewart, B.M., Tingley, D., 2019. stm: An R Package for Structural Topic Models. J. Stat. Soft. 91. https://doi.org/10.18637/jss.v091.i02**
- Roberts, M.E., Stewart, B.M., Airoldi, E.M., 2016a. A Model of Text for Experimentation in the Social Sciences. Journal of the American Statistical Association 111, 988–1003. https://doi.org/10.1080/01621459.2016.1141684
- Roberts, M.E., Stewart, B.M., Tingley, D., 2016b. Navigating the Local Modes of Big Data: The Case of Topic Models, in: Alvarez, R.M. (Ed.), Computational Social Science. Cambridge University Press, Cambridge, pp. 51–97. https://doi.org/10.1017/CBO9781316257340.004
- Roberts, M.E., Stewart, B.M., Tingley, D., Lucas, C., Leder‐Luis, J., Gadarian, S.K., Albertson, B., Rand, D.G., 2014. Structural Topic Models for Open‐Ended Survey Responses. American Journal of Political Science 58, 1064–1082. https://doi.org/10.1111/ajps.12103
- Rodriguez, M.Y., Storer, H., 2020. A computational social science perspective on qualitative data exploration: Using topic models for the descriptive analysis of social media data*. Journal of Technology in Human Services 38, 54–86. https://doi.org/10.1080/15228835.2019.1616350
- Schofield, A., Mimno, D., 2016. Comparing Apples to Apple: The Effects of Stemmers on Topic Models. Transactions of the Association for Computational Linguistics 4, 287–300. https://transacl.org/ojs/index.php/tacl/article/view/868


##### *Published applications of Structural Topic Modelling*

- Curry, T.A., Fix, M.P., 2019. May it please the twitterverse: The use of Twitter by state high court judges. Journal of Information Technology & Politics 16, 379–393. https://doi.org/10.1080/19331681.2019.1657048
- Fischer-Preßler, D., Schwemmer, C., Fischbach, K., 2019. Collective sense-making in times of crisis: Connecting terror management theory with Twitter user reactions to the Berlin terrorist attack. Computers in Human Behavior 100, 138–151. https://doi.org/10.1016/j.chb.2019.05.012
- Lucas, C., Nielsen, R.A., Roberts, M.E., Stewart, B.M., Storer, A., Tingley, D., 2015. Computer-Assisted Text Analysis for Comparative Politics. Political Analysis 23, 254–277. https://doi.org/10.1093/pan/mpu019
- Mishler, A., Crabb, E.S., Paletz, S., Hefright, B., Golonka, E., 2015. Using Structural Topic Modeling to Detect Events and Cluster Twitter Users in the Ukrainian Crisis, in: Stephanidis, C. (Ed.), HCI International 2015 - Posters’ Extended Abstracts, Communications in Computer and Information Science. Springer International Publishing, Cham, pp. 639–644. https://doi.org/10.1007/978-3-319-21380-4_108
- Moschella, M., Pinto, L., 2019. Central banks’ communication as reputation management: How the Fed talks under uncertainty. Public Administration 97, 513–529. https://doi.org/10.1111/padm.12543
- Roberts, M.E., Stewart, B.M., Airoldi, E.M., 2016. A Model of Text for Experimentation in the Social Sciences. Journal of the American Statistical Association 111, 988–1003. https://doi.org/10.1080/01621459.2016.1141684
- Tvinnereim, E., Fløttum, K., 2015. Explaining topic prevalence in answers to open-ended survey questions about climate change. Nature Clim Change 5, 744–747. https://doi.org/10.1038/nclimate2663
- More examples to be found at https://www.structuraltopicmodel.com/. 


##### *Tutorials for Structural Topic Modelling*

- Puschmann, C., 2021. Automatisierte Inhaltsanalyse mit R. http://inhaltsanalyse-mit-r.de/
- Brett, M.R., 2012. Topic Modeling: A Basic Introduction Journal of Digital Humanities. URL http://journalofdigitalhumanities.org/2-1/topic-modeling-a-basic-introduction-by-megan-r-brett/ (accessed 12.23.21).
- FRA, 2019. Messing around with STM, part IIIa: model selection. Seeking the link. URL https://francescocaberlin.blog/2019/06/26/messing-around-with-stm-part-iiia-model-selection/ (accessed 12.23.21).
- Lebryk, T., 2021. Introduction to the Structural Topic Model (STM) [WWW Document]. Medium. URL https://towardsdatascience.com/introduction-to-the-structural-topic-model-stm-34ec4bd5383 (accessed 12.23.21).
- Silge, J., 2018a. Training, evaluating, and interpreting topic models [WWW Document]. Julia Silge. URL https://juliasilge.com/blog/evaluating-stm/ (accessed 12.3.21).
- Silge, J., 2018b. The game is afoot! Topic modeling of Sherlock Holmes stories [WWW Document]. Julia Silge. URL https://juliasilge.com/blog/sherlock-holmes-stm/ (accessed 11.2.21).
- Underwood, T., 2018. Do topic models warp time? The Stone and the Shell. URL https://tedunderwood.com/2018/07/26/do-topic-models-warp-time/ (accessed 12.23.21).
- Unkel, J., 2021. 21 Topic Modeling | Computational Methods in der politischen Kommunikationsforschung. https://bookdown.org/joone/ComputationalMethods/topicmodeling.html 
- Wesslen, R., 2017a. wesslen/text-analysis-org-science. https://github.com/wesslen/text-analysis-org-science
- Wesslen, R., 2017b. wesslen/NCStateSenateFacebook. https://github.com/wesslen/NCStateSenateFacebook 


##### *On R and R packages*

- Chang, W., 2014. R Graphics Cookbook, 2nd edition. O’Reilly. [https://r-graphics.org](https://r-graphics.org)
- **Roberts, M.E., Stewart, B.M., Tingley, D., Benoit, K., 2020. Manual: Package “stm.” https://cran.r-project.org/web/packages/stm/stm.pdf**
- Silge, J., Robinson, D., 2017. Text Mining with R: A Tidy Approach. O’Reilly. [https://www.tidytextmining.com/](https://www.tidytextmining.com/)
- Unkel, J., 2020. Computational Methods in der politischen Kommunikationsforschung. https://bookdown.org/joone/ComputationalMethods/
- Wickham, H., Grolemund, G., 2017. R for Data Science. O’Reilly. [https://r4ds.had.co.nz/](https://r4ds.had.co.nz/)
- Wickham, H., Navarro, D., Pederson, T.L., 2021. ggplot2: elegant graphics for data analysis, On-line version. ed. Springer. [https://ggplot2-book.org/](https://ggplot2-book.org/)
