---
title: "Privacy"
date: "2021-03-18"
layout: single
---

This privacy policy explains the collection of personal data during your visit of HTML pages under the domain *matt-meller.com / matt-meller.de*.

First of all, you do not need to expose any personal data to access *matt-meller.com / matt-meller.de*. Personal data are neither collected nor even asked for. Furthermore, these pages neither embed ads, nor trackers, nor social media plugins, and they are served without the use of cookies.

However, most pages are served as HTML presentations by web servers operated by [GitLab, Inc., 268 Bush Street, Suite 350, San Francisco, CA 94104](https://about.gitlab.com), and your web browser quite likely shares lots of data with GitLab’s web servers, most notably your IP address but also other technical information which can be used to uniquely track you. GitLab explains their use of such data in their [privacy policy](https://about.gitlab.com/privacy/).

This website is encrypted with an SSL certificate issued by Let's Encrypt. Be sure that the address of this website is always prefixed by **https://** and your browser shows the lock symbol in front of the URL.

*Source: eRecht24.de, [Jens Lechtenbörger](https://lechten.gitlab.io/imprint.html)*
