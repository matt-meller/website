---
title: "Joining the Centre for Science and Policy at Cambridge"
date: 2021-04-19T08:08:23+02:00
# categories: ["Working days"]
# tags: ["work", "day"]
# description: "This is meta description"
# keywords: "meta keywords for content"
draft: false
---

For the next ten weeks I have the privilege to join the [Centre for Science and Policy (CSaP)](https://www.csap.cam.ac.uk/) at the University of Cambridge as a policy intern. CSaP is a special institution at Cambridge: it is not a think tank or an agenda-setting institute. Instead, it is a matchmaker in the best sense of it.

It fosters an exchange between policymakers from UK government or decision-makers from the third or private sector with academics from Cambridge, Oxford and beyond. The key however is, this exchange is “demand-driven”. It’s not Science (with a capital-S) telling decision-makers how public policy needs to be. Policymakers and decision-makers come with their pressing, open-ended questions on complex issues at the frontiers of knowledge, and CSaP will help them finding the right set of people with expertise to exchange with.

Questions [may range](https://www.csap.cam.ac.uk/policy-fellowships/faqs/policy-fellow-faqs/some-typical-fellows-questions/) from what measures may best to contribute to a more equal social and economic development of regions, how to get better in technology foresight, to why does the public understand and accept forms of scientific advise differently? This is markedly different from classical science advisory, which works more in the direction of providing answers to (more or less) well-defined problems. CSaP’s aim instead is to “improve the use of evidence and expertise” in policy making, by helping to build meaningful relationships on eye-level.

The pandemic has underscored our time’s interdependence on scientific-technological expertise. Never were answers to questions on any level and scale (How does the virus spread? Does it help to wear masks? How will the pandemic affect our economies? Which risks are acceptable?) so pressing and had so immediate impact on public policy from day 1 on. Furthermore, unlike ever before the analysis, visualisation, and interpretation of data and scientific metrics (incidence numbers, R0, …) suddenly became the pinnacle of public debate and everyday chats. 

It is thus an exciting time to learn more about the science–policy interface, and in the spirit of [Rittel and Weber (1973) what ways there are to challenge wicked problems](http://link.springer.com/10.1007/BF01405730) through evidence and trusting relationships. I am also grateful for the opportunity to get an understanding what UK policy makers move — in particular against the backdrop of my recent experience as policy analyst intern at a Ministry for Digital Affairs, and the UK’s latest [“Integrated Review”](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/975077/Global_Britain_in_a_Competitive_Age-_the_Integrated_Review_of_Security__Defence__Development_and_Foreign_Policy.pdf) setting forth the aim for UK to become a “science superpower”.
